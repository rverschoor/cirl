# frozen_string_literal: true

# Reporting database statistics
class Report
  def initialize
    @daterange = ('2021-02'..'2021-09')
    @metrics = {}
    @metrics[:daterange] = @daterange
  end

  def metrics
    nr_issues_slurped
    nr_issues_reviewed
    nr_created_with_license
    nr_created_with_label
    nr_created_without_label
    nr_licenses_created
    nr_created_issues_per_month(@daterange)
    nr_closed_issues_per_month(@daterange)
    nr_extra_issues
    nr_licenses_per_issue
    @metrics
  end

  def nr_issues_slurped
    @metrics[:nr_issues_slurped] = DB.get_first_value('SELECT COUNT(*) FROM issues;')
  end

  def nr_issues_reviewed
    @metrics[:nr_issues_reviewed] = DB.get_first_value('SELECT COUNT(*) FROM issues WHERE licensesCreated IS NOT NULL;')
  end

  def nr_created_with_license
    @metrics[:nr_created_with_license] = DB.get_first_value('SELECT COUNT(*) FROM issues WHERE licensesCreated > 0;')
  end

  def nr_created_with_label
    @metrics[:nr_created_with_label] = DB.get_first_value('SELECT COUNT(*) FROM issues WHERE licensesCreated > 0 AND licenseLabel = TRUE;')
    @metrics[:nr_created_with_label_percentage] = (@metrics[:nr_created_with_label] * 100) / @metrics[:nr_created_with_license]
  end

  def nr_created_without_label
    @metrics[:nr_created_without_label] = DB.get_first_value('SELECT COUNT(*) FROM issues WHERE licensesCreated > 0 AND licenseLabel = FALSE;')
    @metrics[:nr_created_without_label_percentage] = (@metrics[:nr_created_without_label] * 100) / @metrics[:nr_created_with_license]
  end

  def nr_licenses_created
    @metrics[:nr_licenses_created] = DB.get_first_value('SELECT SUM(licensesCreated) FROM issues;')
  end

  def nr_created_issues_per_month(months)
    @metrics[:nr_created_issues_per_month] = {}
    @metrics[:nr_created_issues_with_license_per_month] = {}
    @metrics[:nr_licenses_in_created_issues_per_month] = {}
    @metrics[:nr_created_issues_extra] = {}
    months.each do |month|
      @metrics[:nr_created_issues_per_month][month] = DB.get_first_value("SELECT COUNT(*) FROM issues WHERE createdAt BETWEEN '#{month}-01' AND '#{month}-31'")
      @metrics[:nr_created_issues_with_license_per_month][month] = DB.get_first_value("SELECT COUNT(*) FROM issues WHERE licensesCreated > 0 AND createdAt BETWEEN '#{month}-01' AND '#{month}-31'")
      @metrics[:nr_licenses_in_created_issues_per_month][month] = DB.get_first_value("SELECT SUM(licensesCreated) FROM issues WHERE createdAt BETWEEN '#{month}-01' AND '#{month}-31'") || 0
      @metrics[:nr_created_issues_extra][month] = @metrics[:nr_licenses_in_created_issues_per_month][month] - @metrics[:nr_created_issues_with_license_per_month][month]
    end
  end

  def nr_closed_issues_per_month(months)
    @metrics[:nr_closed_issues_per_month] = {}
    @metrics[:nr_closed_issues_with_license_per_month] = {}
    @metrics[:nr_licenses_in_closed_issues_per_month] = {}
    @metrics[:nr_closed_issues_extra] = {}
    months.each do |month|
      @metrics[:nr_closed_issues_per_month][month] = DB.get_first_value("SELECT COUNT(*) FROM issues WHERE closedAt BETWEEN '#{month}-01' AND '#{month}-31'")
      @metrics[:nr_closed_issues_with_license_per_month][month] = DB.get_first_value("SELECT COUNT(*) FROM issues WHERE licensesCreated > 0 AND closedAt BETWEEN '#{month}-01' AND '#{month}-31'")
      @metrics[:nr_licenses_in_closed_issues_per_month][month] = DB.get_first_value("SELECT SUM(licensesCreated) FROM issues WHERE closedAt BETWEEN '#{month}-01' AND '#{month}-31'") || 0
      @metrics[:nr_closed_issues_extra][month] = @metrics[:nr_licenses_in_closed_issues_per_month][month] - @metrics[:nr_closed_issues_with_license_per_month][month]
    end
  end

  def nr_extra_issues
    @metrics[:nr_extra_issues] = @metrics[:nr_licenses_created] - @metrics[:nr_created_with_license]
    @metrics[:nr_effective_issues] = @metrics[:nr_issues_reviewed] + @metrics[:nr_extra_issues]
    @metrics[:effective_issues_percentage] = (@metrics[:nr_extra_issues] * 100) / @metrics[:nr_issues_reviewed]
  end

  def nr_licenses_per_issue
    @metrics[:nr_licenses_per_issue] = {}
    max = DB.get_first_value('SELECT MAX(licensesCreated) FROM issues WHERE licensesCreated > 0;')
    @metrics[:nr_licenses_per_issue][:max] = max
    (0..max).each do |nr|
      @metrics[:nr_licenses_per_issue][nr] = DB.get_first_value("SELECT COUNT(*) FROM issues WHERE licensesCreated = #{nr};")
    end
  end
end
