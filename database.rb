# frozen_string_literal: true

require 'sqlite3'

DB_NAME = 'cirl.db'

# Database storage
class Database
  def initialize
    create_database
    create_table
  end

  def create_database
    @db = SQLite3::Database.open DB_NAME
    @db.results_as_hash = true
  end

  def create_table
    @db.execute 'CREATE TABLE IF NOT EXISTS issues(' \
      'iid INT UNIQUE,' \
      'url TEXT,' \
      'createdAt TEXT,' \
      'closedAt TEXT,' \
      'licenseLabel INT,' \
      'licensesCreated INT DEFAULT NULL)'
  end

  def store_issue(issue)
    puts "Store #{issue[:iid]} #{issue[:closedAt]}"
    @db.execute 'INSERT OR IGNORE INTO issues (iid, url, createdAt, closedAt, licenseLabel) VALUES (?, ?, ?, ?, ?)',
                issue[:iid], issue[:url], issue[:createdAt], issue[:closedAt], issue[:licenseLabel] ? 1 : 0
  end

  def store_issue_licenses(iid, nrlicenses)
    puts "Update #{iid} with #{nrlicenses} licenses"
    @db.query "UPDATE issues SET licensesCreated=#{nrlicenses} WHERE iid=#{iid}"
  end

  def query(query)
    @db.query query
  end

  def get_first_value(query)
    @db.get_first_value query
  end
end
