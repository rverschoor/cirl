#!/usr/bin/env ruby
# frozen_string_literal: true

# Required environment variables:
# - CIRL_API_TOKEN=abc123

require 'pp'
require 'sinatra'
require_relative 'database'
require_relative 'query'
require_relative 'issues'
require_relative 'report'

configure do
  DB = Database.new
end

get '/' do
  @nr_issues = DB.get_first_value('SELECT COUNT(*) FROM issues;')
  @nr_unreviewed_issues = DB.get_first_value('SELECT COUNT(*) FROM issues WHERE licensesCreated IS NULL;')
  @first_date = DB.get_first_value('SELECT MIN(createdAt) FROM issues;')
  @last_date = DB.get_first_value('SELECT MAX(createdAt) FROM issues;')
  erb :index
end

get '/list' do
  @issues = DB.query('SELECT * FROM issues;')
  erb :list
end

get '/report' do
  @report = Report.new
  @metrics = @report.metrics
  erb :report
end

get '/review' do
  result = DB.query('SELECT * FROM issues WHERE licensesCreated IS NULL LIMIT 1;')
  @issue = result.next
  if @issue
    erb :review
  else
    redirect '/'
  end
end

post '/review' do
  iid = Integer(params['issue_iid'])
  nrlicenses = Integer(params['nr_licenses'])
  DB.store_issue_licenses(iid, nrlicenses)
  redirect '/review'
end

get '/slurp' do
  erb :slurp
end

post '/slurp' do
  @issues = Issues.new DB
  @query = Query.new PROJECT, @issues
  slurp_date = params['slurp_date']
  puts "Slurping issues since #{slurp_date}"
  @query.retrieve_issues slurp_date
  redirect '/list'
end
