# frozen_string_literal: true

require 'faraday'
require 'json'

PROJECT = 'gitlab-com/support/internal-requests'
PAGINATION_SIZE = 100

# Query class performs the GraphQL call to retrieve issues
class Query
  def initialize(project, issues)
    @project = project
    @issues = issues
    api_token = ENV['CIRL_API_TOKEN']
    init_api_connection api_token
  end

  def init_api_connection(token)
    @api_connection = Faraday.new(
      url: 'https://gitlab.com/api/graphql',
      headers: {
        'Content-Type' => 'application/json',
        'Authorization' => "Bearer #{token}"
      }
    )
  end

  def graphql_query(slurp_date, cursor)
    <<~GRAPHQL
      {
        project(fullPath: "#{@project}") {
          name
          issues(
            state: closed,
            first: #{PAGINATION_SIZE},
            after: "#{cursor}",
            labelName: ["License Issue", "Platform::Self-Managed"],
            not: {
              labelName: ["Support::US-Federal"],
            }
            createdAfter: "#{slurp_date}"
            ) {
            count
            pageInfo {
              hasNextPage
              endCursor
            }
            nodes {
              iid
              webUrl
              title
              createdAt
              closedAt
              labels {
                nodes {
                  title
                }
              }
            }
          }
        }
      }
    GRAPHQL
  end

  def run(slurp_date, cursor)
    query = { query: graphql_query(slurp_date, cursor) }
    response = @api_connection.post('', query.to_json)
    process_response response
  end

  def process_response(response)
    if response.status != 200
      puts "Query failed #{response.status} - #{response.body}"
      data = nil
    else
      data = process_response_data(response)
    end
    data
  end

  def process_response_data(response)
    data = JSON.parse(response.body)
    if data.keys[0] == 'errors'
      pp data
      data = nil
    end
    data
  end

  def retrieve_issues(slurp_date)
    end_cursor = ''
    while end_cursor
      data = run slurp_date, end_cursor
      end_cursor = process_issues_data(data)
    end
  end

  def process_issues_data(data)
    end_cursor = nil
    if data && data['data']['project']
      @issues.store(data['data']['project']['issues']['nodes'])
      end_cursor = data['data']['project']['issues']['pageInfo']['endCursor']
    end
    end_cursor
  end
end
