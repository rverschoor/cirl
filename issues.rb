# frozen_string_literal: true

# Issues class collects all issues retrieved from API
class Issues
  attr_reader :issues

  def initialize(database)
    @database = database
    @issues = []
  end

  def store(nodes)
    nodes.each do |node|
      extract_issue node
    end
  end

  def extract_issue(node)
    issue = {}
    issue[:iid] = node['iid']
    issue[:url] = node['webUrl']
    issue[:createdAt] = node['createdAt'][0..9] # Extract yyyy-mm-dd
    issue[:closedAt] = node['closedAt'][0..9]   # Extract yyyy-mm-dd
    issue[:licenseLabel] = license_label? node['labels']['nodes']
    @database.store_issue(issue)
  end

  def license_label?(labels)
    license_labels = ['Manually Generate Trial License', 'Manually Generate License']
    has_label = false
    labels.each do |label|
      has_label = true if license_labels.include? label['title']
    end
    has_label
  end
end
