# Count Internal Requests Licenses

## What
The L&R Support team handles support tickets in Zendesk, and internal requests in GitLab issues. \
The internal requests are often overlooked when the workload of the team is discussed.

When the internal requests are included, it's overlooked that quite a few issues are re-opened to request another license.
I wanted to find out how often issues were re-opened, so I did a post-mortem.

Manually counting re-open events would be too much work, so I choose to count the number of licenses
created in each issue. \
Issues are often re-opened to request an extension of a license, so counting the licenses feels like a good alternative. \
Re-opened issues for which no extra license is generated are not counted, so the actual number of re-opened
issues is higher than what I counted.

I also wanted to show that manual post-mortems are doable.

## How

`cirl` is a simple Ruby/Sinatra webapp. \
Using a GraphQL query it collects all closed issues with labels `License Issue` + `Platform::Self-Managed`, 
excluding `Support::US-Federal` (as I can't review those). \
It only collects non personally identifiable information and stores it in an `SQLite` database.

Through the web interface each issue can be viewed, and then record the number of licenses created in that issue.

# Iffy

- Sometimes a new license was created because the SE had made a mistake, I didn't count these.
- Some internal requests are re-opened over multiple weeks or even months.
As the count is done per issue, and not per re-open event, the workload can't be contributed to an exact time.
- I only reviewed the `License Issue` + `Platform::Self-Managed` issues, all other issues were skipped. 

# Results

- I reviewed 1852 issues from the past 8 months.
- For 1566 of these 1852 issues one or more licenses were created.
- Of the 1566 issues with a license, 1042 issues (66%) had the label 
`Manually Generate Trial License` or 
`Manually Generate License`, 524 issues (33%) were missing a label.
- For the 1566 issues with a license, a total of 1828 licenses were created.
- This implies 1828 - 1566 = 262 re-opens.
- Total number of handled issues is 1852 issues + 262 re-opens = 2114 issues.
- Actual workload is not 1852 but 2214 issues, a 14% increase.

# Report output

### Generic

- Number of issues slurped = 1852
- Number of issues reviewed = 1852

### Reviewed issues

#### Label usage

- Number of issues where license was created = 1566
- Number of issues where license was created with label = 1042 (66%)
- Number of issues where license was created without label = 524 (33%)

#### Effective number of issues

- Number of issues where license was created = 1566
- Number of licenses created = 1828
- Effective number of issues = 2114 (+ 14%)

### Number of licenses per issue

| Licenses | Issues |
|----------|--------|
|0|286|
|1|1369|
|2|155|
|3|29|
|4|6|
|5|4|
|6|3|

### Created issues per month

| Month | Issues created | Issues created with license/s | Licenses in issues created | Effective issues |
|-------|----------------|-------------------------------|----------------------------|------------------|
|2021-02|202|167|197|232 (+30)|
|2021-03|272|242|289|319 (+47)|
|2021-04|257|215|253|295 (+38)|
|2021-05|217|187|219|249 (+32)|
|2021-06|289|239|285|335 (+46)|
|2021-07|229|195|222|256 (+27)|
|2021-08|192|161|181|212 (+20)|
|2021-09|194|160|182|216 (+22)|

### Closed issues per month

| Month | Issues closed | Issues closed with license/s | Licenses in issues closed | Effective issues |
|-------|---------------|------------------------------|---------------------------|------------------|
|2021-02|161|132|146|175 (+14)
|2021-03|267|237|272|302 (+35)
|2021-04|256|214|241|283 (+27)
|2021-05|239|209|244|274 (+35)
|2021-06|270|223|273|320 (+50)
|2021-07|252|211|250|291 (+39)
|2021-08|193|163|189|219 (+26)
|2021-09|214|177|213|250 (+36)

<img src="charts.png" height=400/>